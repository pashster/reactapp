import React, { Component } from 'react';
import request  from 'superagent/superagent'

class Login extends Component {

  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      token: '',
      is_admin: false,
      is_log_in: false
    };

    this.handleChangeEmail = this.handleChangeEmail.bind(this);
    this.handleChangePassword = this.handleChangePassword.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleSubmit(e) {
    e.preventDefault();
    console.log('form submitted email = ' + this.state.email + '. Password = ' + this.state.password);
    this.sendParamsFromServer();
  }

  sendParamsFromServer () {
      var self = this;
      request
          .post('/api/auth/login')
          .set('Content-Type', 'application/json')
          .send({ email: this.state.email, password: this.state.password })
          .end(function(err, res){
              console.log('TOKEN ===> ', res.body.auth_token);
              console.log('IS LOG IN ===> ', res.body.is_log_in);
              console.log('IS ADMIN ===> ', res.body.is_admin);
              self.setState({token: res.body.auth_token, is_admin: res.body.is_admin, is_log_in: res.body.is_log_in})
          });
  }

  allTodos (token) {
      request
          .get('/api/todos')
          .set('Authorization', token)
          .end(function(err, res) {
              console.log(res.text)
          })
  }

  handleChangeEmail(event) {
      this.setState({email: event.target.value});
  }

  handleChangePassword(event) {
      this.setState({password: event.target.value});
  }

  render() {
      console.log(this.state);
    return (
      <div className="ui raised very padded text container segment">
        <form className="ui form" onSubmit={this.handleSubmit}>
          <div className="field">
            <label>Email</label>
            <input
                value={this.state.email}
                onChange={this.handleChangeEmail.bind(this)}
                type="text" name="first-name"
                placeholder="Email ..." />
          </div>
          <div className="field">
            <label>Password</label>
            <input
                value={this.state.password}
                onChange={this.handleChangePassword.bind(this)}
                type="password" name="last-name"
                placeholder="Password ..." />
          </div>
          <div className="field">
            <div className="ui checkbox">
              <input type="checkbox" tabIndex="0" />
              <label>Я согласен с условиями пользовательского соглашения</label>
            </div>
          </div>
          <button className="ui button" type="submit">Submit</button>
        </form>
      </div>
    );
  }
}

export default Login;
