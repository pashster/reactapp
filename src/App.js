import React, { Component } from 'react';
import './App.css';
import Login from './components/auth/login/Login'

class App extends Component {
  render() {
    return (
      <div className="ui container">
        <div className="ui secondary  menu">
          <a className="active item">
            Home
          </a>
          <a className="item">
            Messages
          </a>
          <a className="item">
            Friends
          </a>
          <div className="right menu">
            <a className="ui item">
              Logout
            </a>
          </div>
        </div>

        <div className="ui centered grid">
            <Login />
        </div>
      </div>
    );
  }
}

export default App;
